 <?php
    // require_once('../includes/public-application-header.php');
    $con=mysql_connect("localhost","root",'') or die("Failed to connect with database!!!!");
  mysql_select_db("koan29", $con); 
    $sql   = "select * from reporting_system";
    // $sql  =" SELECT DATE_FORMAT( FROM_UNIXTIME( transaction_date ) , '%d/%m/%Y') 'transaction_date', COUNT(*) 'total count', SUM(transaction_status = 'success') ";
    // $sql .=" success, SUM(transaction_status = 'inprocess') inprocess, SUM(transaction_status = 'fail') fail, ";
    // $sql .=" SUM(transaction_status = 'cancelled') cancelled FROM user_transaction WHERE ";
    // $sql .=" transaction_date >= 1325376000 AND transaction_date <= 1373846400 GROUP BY date(FROM_UNIXTIME(transaction_date)) ";

    $r= mysql_query($sql) or die(mysql_error()); 
    $transactions = array();
    while($result  = mysql_fetch_array($r, MYSQL_ASSOC)){
      $transactions[] = $result;
    } 
    $rows = array();
      //flag is not needed
      $flag = true;
      $table = array();
      $table['cols'] = array(

    // Labels for your chart, these represent the column titles
    // Note that one column is in "string" format and another one is in "number" format as pie chart only required "numbers" for calculating percentage and string will be used for column title
    array('label' => 'Database', 'type' => 'string'),
    array('label' => 'date', 'type' => 'number'),
    array('label' => 'Clicks', 'type' => 'number'),
    //array('label' => 'Impressions', 'type' => 'number'),
    //array('label' => 'Web_sessions', 'type' => 'number'),
    //array('label' => 'M_sessions', 'type' => 'number'),
    
    );
  $rows = array();

  foreach($transactions as $tr) {
    $temp = array();

     foreach($tr as $key=>$value){

    // the following line will be used to slice the Pie chart
    $temp[] = array('v' => (string) $key); 

    // Values of each slice
    $temp[] = array('v' => (int) $value);     
    }
  $rows[] = array('c' => $temp); 
}
    $table['rows'] = $rows;

    $jsonTable = json_encode($table);
    //echo $jsonTable;
?>

<html>
    <head>
    <!--Load the Ajax API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script type="text/javascript">

    // Load the Visualization API and the piechart package.
    google.load('visualization', '1', {'packages':['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      // Create our data table out of JSON data loaded from server.
      var data = new google.visualization.DataTable(<?=$jsonTable?>);
	   var dataTable = new google.visualization.DataTable();
      dataTable.addColumn('number', 'Voltage (V)');
      dataTable.addColumn('number', 'Current (mA.)');
      dataTable.addRows([
        [new Date(2008, 0, 1), 1],
        [new Date(2008, 1, 14), 2],
        [new Date(2008, 9, 14), 12],
        [new Date(2008, 10, 14), 22],
        [new Date(2009, 1, 1), 30]
      ]);

      var dataView = new google.visualization.DataView(dataTable);
      dataView.setColumns([{calc: function(data, row) { return data.getFormattedValue(row, 0); }, type:'string'}, 1]);

      var chart = new google.visualization.LineChart(document.getElementById('containerID'));
      var options = {
        width: 400, height: 240,
        legend: 'none',
        pointSize: 5
      };
      chart.draw(dataView, options);
    }
    </script>
  </head>
  <body>
    <!--this is the div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>
</html>